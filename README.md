# Purpose

Get measurements from some hardware sensors and share them using `prometheus` protocol to integrate with `grafana` visualisation graphs.


# Hardware

Tested on raspberry pi zero.

## Sensors

List of supported sensors with **default** addresses and possible chip alternatives using hardware switches:
- `si7021`: **0x40**
- `tmp102`: **0x48** 0x49
- `ads1115`: **0x48** 0x49 0x4A 0x4B
- `tmp36`: analog


# Dependencies

Install rust compiler and `cargo` build system
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

```

To build crossplatform for raspberry pi zero
```
rustup target add arm-unknown-linux-gnueabi

# Install gcc linker
sudo apt install gcc-arm-linux-gnueabi
```


# Build

## Native host

```
./build
```

Output binary is in path `./target/debug/prometheus_sensors_exporter`


## Crossplatform

Raspbery pi zero
```
./build-cross-rpi-zero
```

Output binary is in path `./target/arm-unknown-linux-gnueabi/debug/prometheus_sensors_exporter`


**NOTE:** Default build profiles are development ones with lots of debug symbols and not optimized.


# Run

## Weather station

To run server with already prepared configuration for weather station
```
./run-weather-station
```

Then to collect measurements
```
% curl localhost:9898
# HELP ads1115_channel0 measurement from ads1115 adc sensor channel 0
# TYPE ads1115_channel0 gauge
ads1115_channel0 -0.125
# HELP ads1115_channel1 measurement from ads1115 adc sensor channel 1
# TYPE ads1115_channel1 gauge
ads1115_channel1 1642.25
# HELP ads1115_channel2 measurement from ads1115 adc sensor channel 2
# TYPE ads1115_channel2 gauge
ads1115_channel2 1642.25
# HELP ads1115_channel3 measurement from ads1115 adc sensor channel 3
# TYPE ads1115_channel3 gauge
ads1115_channel3 742.125
# HELP si7021_humidity humidity from si7021 sensor
# TYPE si7021_humidity gauge
si7021_humidity 35.23
# HELP si7021_temperature temperature from si7021 sensor
# TYPE si7021_temperature gauge
si7021_temperature 25.6
# HELP tmp102 temperature from tmp102 sensor
# TYPE tmp102 gauge
tmp102 25.875
# HELP tmp36 temperature from tmp36 analog sensor attached to adc sensor channel 3
# TYPE tmp36 gauge
tmp36 24.275000000000006
```

## Custom configuration

### Help

To run custom set of sensors firstly take a look at help message
```
% cargo run -- --help
    Finished dev [unoptimized + debuginfo] target(s) in 0.04s
     Running `target/debug/prometheus_sensors_exporter --help`
prometheus_sensors_exporter 0.1.2

USAGE:
    prometheus_sensors_exporter [OPTIONS]... ( SENSOR )...

WHERE:
    SENSOR = sensor --name <name> --description <description> DEVICE
    DEVICE = ( tmp102 I2C | si7021-temperature I2C | si7021-humidity I2C | ads1115-channel0 I2C | ads1115-channel1 I2C | ads1115-channel2 I2C | ads1115-channel3 I2C | tmp36 ADC )
    I2C = --i2c-bus <bus> [ --i2c-address <address> ]
    SPI = --spi-bus <bus> [ --spi-address <address> ]
    ADC = --adc-channel <name of previously defined adc sensor channel>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
        --dry-run    Test correctness of passed command line arguments

OPTIONS:
        --socket-interface <socket-interface>        Address of interface to bind [default: 0.0.0.0]
        --socket-port <socket-port>                  Number in decimal [default: 9898]
```

### Examples

It is required to define at least one sensor like
```
% cargo run -- \
    sensor \
        --name \
            sensor_tmp102 \
        --description \
            "temperature from tmp102 sensor" \
        tmp102 \
            --i2c-bus \
                0 \
```

To get measurement from two similar sensors connected on different i2c buses
```
% cargo run -- \
    sensor \
        --name \
            sensor_tmp102_indoor \
        --description \
            "temperature from tmp102 sensor indoor" \
        tmp102 \
            --i2c-bus \
                0 \
    sensor \
        --name \
            sensor_tmp102_indoor \
        --description \
            "temperature from tmp102 sensor outdoor" \
        tmp102 \
            --i2c-bus \
                1 \

```

Similar sensors could be connected on same i2c bus, but hardware switches on them would use different i2c addreses. When i2c address is different than default, then it have to be specified.
```
% cargo run -- \
    sensor \
        --name \
            sensor_tmp102_indoor \
        --description \
            "temperature from tmp102 sensor indoor" \
        tmp102 \
            --i2c-bus \
                0 \
            --i2c-address \
                $(printf "%d\n" 0x48) \
    sensor \
        --name \
            sensor_tmp102_indoor \
        --description \
            "temperature from tmp102 sensor outdoor" \
        tmp102 \
            --i2c-bus \
                0 \
            --i2c-address \
                $(printf "%d\n" 0x49) \
```

One device could provide few different measuremnts. Each measurement is treated as separate sensor. These sensors on the same device have defined the same hardware interface options.
```
% cargo run -- \
    sensor \
        --name \
            sensor_si7021_temperature \
        --description \
            "temperature from si7021 sensor" \
        si7021-temperature \
            --i2c-bus \
                0 \
    sensor \
        --name \
            sensor_si7021_humidity \
        --description \
            "humidity from si7021 sensor" \
        si7021-humidity \
            --i2c-bus \
                0 \
```

Analog-to-digital converter `ads1115` has 4 physical channels to measure which means there are 4 sensors. These sensors are printing raw values. Analog sensors could be chained with them by referring corresponding name of it's adc channel
```
% cargo run -- \
    sensor \
        --name \
            sensor_adc_channel_2 \
        --description \
            "measurement from ads1115 adc sensor channel 2"\
        ads1115-channel2 \
            --i2c-bus \
                1 \
    sensor \
        --name \
            some_analog_sensor \
        --description \
            some-description-for-analog-sensor \
        tmp36 \
            --adc-channel \
                adc_channel_2 \
```


# Test

To run tests
```
cargo test
```


# Formatting

Source code formatting feature to ignore external sources is used which requires nightly toolchain just for that.

To install nightly toolchain
```
rustup toolchain install nightly
```

Then apply automatic formatting for source code
```
cargo +nightly fmt
```


# Versioning

Project follows [Semantic Versioning](https://semver.org/) rules.


# Licenses

Project is under [Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.txt)

## External resources

Used resources which are not managed by cargo build system:
- [si7021](https://github.com/wose/si7021/) is [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0) or [MIT](http://opensource.org/licenses/MIT)
- [Logo](https://www.svgrepo.com/svg/81842/mercury-thermometer-with-sun) is [Creative Commons BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode.txt)
