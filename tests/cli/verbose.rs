use assert_cmd::prelude::*; // Add methods on commands
use std::process::Command; // Run programs // Used for writing assertions


#[test]

fn version() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Provide parameter
    cmd.arg("--version");

    // Ensure expected results
    cmd.assert().success();

    Ok(())
}


#[test]

fn version_short_option() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Provide parameter
    cmd.arg("-V");

    // Ensure expected results
    cmd.assert().success();

    Ok(())
}
