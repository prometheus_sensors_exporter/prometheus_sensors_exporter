use assert_cmd::prelude::*; // Add methods on commands
use predicates::prelude::*;
use std::process::Command; // Run programs // Used for writing assertions


#[test]

fn custom_valid_interface_loopback_127_0_0_1() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define arguments
    let arguments = "
            --dry-run
                true

            --socket-interface
                127.0.0.1

            sensor
                --name
                    tmp102
                --description
                    temperature-from-tmp102-sensor
                tmp102
                    --i2c-bus
                        0
        ";

    // Pass arguments
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert()
        .success()
        .stdout(predicate::str::contains("127.0.0.1:9898"));

    Ok(())
}


#[test]

fn custom_valid_port_10000() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define arguments
    let arguments = "
            --dry-run
                true

            --socket-port
                10000

            sensor
                --name
                    tmp102
                --description
                    temperature-from-tmp102-sensor
                tmp102
                    --i2c-bus
                        0
        ";

    // Pass arguments
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert()
        .success()
        .stdout(predicate::str::contains("0.0.0.0:10000"));

    Ok(())
}


#[test]

fn interface_invalid() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define arguments
    let arguments = "
            --dry-run
                true

            --socket-interface
                INVALID
        ";

    // Pass invalid arguments
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "Cannot parse server address and port to bind",
    ));

    Ok(())
}


#[test]

fn port_out_of_range() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define arguments
    let arguments = "
            --dry-run
                true

            --socket-port
                100000
        ";

    // Pass invalid arguments
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "number too large to fit in target type",
    ));

    Ok(())
}
