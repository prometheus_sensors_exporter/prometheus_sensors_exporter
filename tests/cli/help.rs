use assert_cmd::prelude::*; // Add methods on commands
use predicates::prelude::*;
use std::process::Command; // Run programs // Used for writing assertions


#[test]

fn help() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Provide parameter
    cmd.arg("--help");

    // Ensure expected results
    cmd.assert()
        .success()
        .stdout(predicate::str::contains("USAGE"));

    Ok(())
}


#[test]

fn help_short_option() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Provide parameter
    cmd.arg("-h");

    // Ensure expected results
    cmd.assert()
        .success()
        .stdout(predicate::str::contains("USAGE"));

    Ok(())
}
