use assert_cmd::prelude::*; // Add methods on commands
use predicates::prelude::*;
use std::process::Command; // Run programs // Used for writing assertions


#[test]

fn one_mandatory_sensor() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Do not pass any argument

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "Command line arguments have to define at least one mandatory sensor",
    ));

    Ok(())
}


#[test]

fn one_sensor_tmp102() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    tmp102
                --description
                    temperature-from-tmp102-sensor
                tmp102
                    --i2c-bus
                        0
        ";

    // Pass sensors with exactly same name
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().success();

    Ok(())
}


#[test]

fn one_sensor_name_have_to_be_provided() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --description
                    temperature-from-tmp102-sensor
                tmp102
                    --i2c-bus
                        0
        ";

    // Pass sensor without specified required --i2c-bus
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "The following required arguments were not provided",
    ));

    Ok(())
}


#[test]

fn one_sensor_description_have_to_be_provided() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    tmp102
                tmp102
                    --i2c-bus
                        0
        ";

    // Pass sensor without specified required --i2c-bus
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "The following required arguments were not provided",
    ));

    Ok(())
}


#[test]

fn one_sensor_i2c_bus_have_to_be_provided() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    tmp102
                --description
                    temperature-from-tmp102-sensor
                tmp102
        ";

    // Pass sensor without specified required --i2c-bus
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "The following required arguments were not provided",
    ));

    Ok(())
}


#[test]

fn one_sensor_few_fields_missing_which_have_to_be_provided(
) -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                tmp102
                    --i2c-bus
                        0
        ";

    // Pass sensor without specified required --i2c-bus
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "The following required arguments were not provided",
    ));

    Ok(())
}


#[test]

fn one_sensor_i2c_bus_have_to_be_number() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    tmp102
                --description
                    temperature-from-tmp102-sensor
                tmp102
                    --i2c-bus
                        SHOULD_BE_NUMBER
        ";

    // Pass sensor without specified required --i2c-bus
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert()
        .failure()
        .stdout(predicate::str::contains("invalid digit found in string"));

    Ok(())
}


#[test]

fn one_sensor_i2c_address() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    tmp102
                --description
                    temperature-from-tmp102-sensor
                tmp102
                    --i2c-bus
                        0
                    --i2c-address
                        1
        ";

    // Pass sensors with exactly same name
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().success();

    Ok(())
}


#[test]

fn one_sensor_i2c_address_have_to_be_number() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    tmp102
                --description
                    temperature-from-tmp102-sensor
                tmp102
                    --i2c-bus
                        0
                    --i2c-address
                        SHOULD_BE_NUMBER
        ";

    // Pass sensor without specified required --i2c-bus
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert()
        .failure()
        .stdout(predicate::str::contains("invalid digit found in string"));

    Ok(())
}


#[test]

fn one_sensor_i2c_address_out_of_range_257() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    tmp102
                --description
                    temperature-from-tmp102-sensor
                tmp102
                    --i2c-bus
                        0
                    --i2c-address
                        257
        ";

    // Pass sensors with exactly same name
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "number too large to fit in target typ",
    ));

    Ok(())
}


#[test]
// TODO
#[ignore]

fn one_sensor_i2c_address_out_of_range_129() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    tmp102
                --description
                    temperature-from-tmp102-sensor
                tmp102
                    --i2c-bus
                        0
                    --i2c-address
                        129
        ";

    // Pass sensors with exactly same name
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "number too large to fit in target typ",
    ));

    Ok(())
}


#[test]

fn two_sensors() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    si7021_temperature
                --description
                    temperature-from-si7021-sensor
                si7021-temperature
                    --i2c-bus
                        0

            sensor
                --name
                    si7021_humidity
                --description
                    humidity-from-si7021-sensor
                si7021-humidity
                    --i2c-bus
                        0
        ";

    // Pass sensor without specified required --i2c-bus
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().success();

    Ok(())
}


#[test]

fn unique_sensors_names() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    NAME_SHOULD_BE_UNIQUE
                --description
                    some-description-for-first-sensor
                tmp102
                    --i2c-bus
                        0

            sensor
                --name
                    NAME_SHOULD_BE_UNIQUE
                --description
                    some-description-for-second-sensor
                tmp102
                    --i2c-bus
                        1
        ";

    // Pass sensors with exactly same name
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "All sensors name have to be unique, but at least one is not",
    ));

    Ok(())
}


#[test]

fn proper_analog_sensor_with_adc() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    adc_channel_0
                --description
                    some-description-for-adc
                ads1115-channel0
                    --i2c-bus
                        1

            sensor
                --name
                    some_analog_sensor
                --description
                    some-description-for-analog-sensor
                tmp36
                    --adc-channel
                        adc_channel_0
        ";

    // Pass sensors with exactly same name
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().success();

    Ok(())
}


#[test]

fn analog_sensor_assigned_channel() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    tmp102
                --description
                    some-description
                tmp36
                    --adc-channel
                        CORRESPONDING_ADC_CHANNEL_IS_NOT_DEFINED
        ";

    // Pass analog sensor without assigned channel to previously defined adc sensor
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "is referring to adc channel of not defined sensor",
    ));

    Ok(())
}


#[test]

fn analog_sensors_chain_with_adc() -> Result<(), Box<dyn std::error::Error>> {

    // Main executable
    let mut cmd = Command::cargo_bin("prometheus_sensors_exporter")?;

    // Define sensors
    let arguments = "
            --dry-run
                true

            sensor
                --name
                    adc_channel_0
                --description
                    some-description-for-adc
                ads1115-channel0
                    --i2c-bus
                        1

            sensor
                --name
                    intermediate_analog_sensor
                --description
                    description
                tmp36
                    --adc-channel
                        adc_channel_0

            sensor
                --name
                    chained-analog-sensor
                --description
                    description
                tmp36
                    --adc-channel
                        intermediate_analog_sensor
        ";

    // Pass sensors with exactly same name
    cmd.args(arguments.split_whitespace());

    // Ensure expected results
    cmd.assert().failure().stdout(predicate::str::contains(
        "should refer to adc channel but it is referring to another chained analog sensor",
    ));

    Ok(())
}
