mod cli {

    mod help;
    mod verbose;
    mod sensors;
    mod socket;
}
