// Standard modules paths
use std::env;
use std::process::exit;

// Declare modules
mod api;
mod board;
mod config;
mod external;
mod implementation;
mod server;

#[macro_use(block)]
extern crate nb;


// Main program logic
fn run() -> Result<(), Box<dyn std::error::Error>> {

    // Initialize logger
    env_logger::init();

    // Parse configuration
    let config = config::parse_and_validate(env::args())?;

    // Initialize board
    let board = board::init(config.sensors)?;

    // Run server
    server::run(config.socket, board);

    // Return success
    Ok(())
}

// Program entrypoint
fn main() {

    // Run main logic and handle possible error
    match run() {
        Ok(..) => {},
        Err(error) => {

            // Print error to standard error output
            println!("{}", error);

            // Exit failure
            exit(1);
        },
    }
}
