// Generic sensor interface to read measurement

pub trait Sensor {
    type Output;

    fn name(&self) -> String;

    fn description(&self) -> String;

    fn read(&mut self) -> Result<Self::Output, String>;
}
