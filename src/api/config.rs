// Standard modules paths
use std::net::SocketAddr;

// External modules paths
use structopt::StructOpt;


// Hardware interfaces

#[derive(StructOpt, PartialEq, Clone, Debug)]

pub struct I2c {
    /// Number in decimal
    #[structopt(long)]
    pub i2c_bus: usize,

    // TODO it should be only 0 - 127
    /// Number in hexadecimal
    #[structopt(long)]
    pub i2c_address: Option<u8>,
}

#[derive(StructOpt, PartialEq, Clone, Debug)]

pub struct Spi {
    /// Number in decimal
    #[structopt(long)]
    pub spi_bus: usize,

    /// Number in hexadecimal
    #[structopt(long)]
    pub spi_address: Option<u8>,
}

#[derive(StructOpt, PartialEq, Clone, Debug)]

pub struct Adc {
    /// Name of previously defined adc sensor channel
    #[structopt(long)]
    pub adc_channel: String,
}


#[derive(StructOpt, PartialEq, Clone, Debug)]

// TODO not used so far
pub enum HardwareInterface {
    I2c(I2c),
    Spi(Spi),
}


// Devices

#[derive(StructOpt, PartialEq, Clone, Debug)]

pub struct Tmp102 {
    #[structopt(flatten)]
    pub interface: I2c,
}


#[derive(StructOpt, PartialEq, Clone, Debug)]

pub struct Si7021 {
    #[structopt(flatten)]
    pub interface: I2c,
}


#[derive(StructOpt, PartialEq, Clone, Debug)]

pub struct Ads1115 {
    #[structopt(flatten)]
    pub interface: I2c,
}


#[derive(StructOpt, PartialEq, Clone, Debug)]

pub struct Tmp36 {
    #[structopt(flatten)]
    pub interface: Adc,
}


#[derive(StructOpt, PartialEq, Clone, Debug)]

pub enum Device {
    Tmp102(Tmp102),
    Si7021Temperature(Si7021),
    Si7021Humidity(Si7021),
    Ads1115Channel0(Ads1115),
    Ads1115Channel1(Ads1115),
    Ads1115Channel2(Ads1115),
    Ads1115Channel3(Ads1115),
    Tmp36(Tmp36),
}


#[derive(Debug, PartialEq, Clone, StructOpt)]

pub struct ConfigSensor {
    /// String shown in output
    #[structopt(long)]
    pub name: String,

    /// String shown in output
    #[structopt(long)]
    pub description: String,

    #[structopt(subcommand)]
    pub device: Device,
}


#[derive(Clone)]

pub struct Config {
    // Network interface and tcp port to listen for new connections
    pub socket: SocketAddr,

    // Dry run flag to exit program earlier after arguments parsing
    pub dry_run: bool,

    // Sensors
    pub sensors: Vec<crate::api::config::ConfigSensor>,
}
