// Standard modules paths
use std::error;
use std::fmt;
use std::net;


#[derive(Debug)]
pub enum CliError {
    // Cannot access clap internal error structure as it is private in current 2.33 version
    ParseCli(String),
    ParseSocket(net::AddrParseError),
}


// Add empty Error trait
impl error::Error for CliError {}


// Implement Display trait
impl fmt::Display for CliError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            CliError::ParseCli(ref err) => write!(f, "Cannot parse provided command line arguments: {}", err),
            CliError::ParseSocket(ref err) => write!(f, "Cannot parse server address and port to bind: {}", err),
        }
    }
}


impl From<net::AddrParseError> for CliError {
    fn from(err: net::AddrParseError) -> CliError {
        CliError::ParseSocket(err)
    }
 }


// Implement conversion from String to custom string wrapper
impl From<String> for CliError {
    fn from(err: String) -> CliError {
        CliError::ParseCli(err)
    }
 }
