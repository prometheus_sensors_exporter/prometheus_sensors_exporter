// Internal modules paths
use crate::api::sensor::Sensor;
use crate::api::config::ConfigSensor;
use crate::api::config::Device;
use crate::implementation::sensor::ads1115;
use crate::implementation::sensor::si7021;
use crate::implementation::sensor::tmp102::Tmp102;
use crate::implementation::sensor::tmp36::Tmp36;


fn create_sensor_based_on_configuration(
    config_sensor: &ConfigSensor,
    config_sensors: &Vec<ConfigSensor>,
) -> Result<Box<dyn Sensor<Output = f64> + Send + Sync>, Box<dyn std::error::Error>> {

    // Dispatch given sensor configuration and initialize sensor
    match &config_sensor.device {
        // tmp102
        Device::Tmp102(tmp102) => {
            Ok(Box::new(Tmp102::init(
                config_sensor.name.clone(),
                config_sensor.description.clone(),
                tmp102.interface.i2c_bus,
                tmp102.interface.i2c_address,
            )?))
        }

        // si7021 temperature
        Device::Si7021Temperature(si7021) => {

            let mut si7021 = si7021::init(
                config_sensor.name.clone(),
                config_sensor.description.clone(),
                si7021.interface.i2c_bus,
                si7021.interface.i2c_address,
            )?;

            Ok(Box::new(si7021.temperature()))
        }

        // si7021 humidity
        Device::Si7021Humidity(si7021) => {

            let mut si7021 = si7021::init(
                config_sensor.name.clone(),
                config_sensor.description.clone(),
                si7021.interface.i2c_bus,
                si7021.interface.i2c_address,
            )?;

            Ok(Box::new(si7021.humidity()))
        }

        // ads1115
        Device::Ads1115Channel0(ads1115) => {

            let mut ads1115 = ads1115::init(
                config_sensor.name.clone(),
                config_sensor.description.clone(),
                ads1115.interface.i2c_bus,
                ads1115.interface.i2c_address,
            )?;

            Ok(Box::new(ads1115.channel0()))
        }

        // ads1115
        Device::Ads1115Channel1(ads1115) => {

            let mut ads1115 = ads1115::init(
                config_sensor.name.clone(),
                config_sensor.description.clone(),
                ads1115.interface.i2c_bus,
                ads1115.interface.i2c_address,
            )?;

            Ok(Box::new(ads1115.channel1()))
        }

        // ads1115
        Device::Ads1115Channel2(ads1115) => {

            let mut ads1115 = ads1115::init(
                config_sensor.name.clone(),
                config_sensor.description.clone(),
                ads1115.interface.i2c_bus,
                ads1115.interface.i2c_address,
            )?;

            Ok(Box::new(ads1115.channel2()))
        }

        // ads1115
        Device::Ads1115Channel3(ads1115) => {

            let mut ads1115 = ads1115::init(
                config_sensor.name.clone(),
                config_sensor.description.clone(),
                ads1115.interface.i2c_bus,
                ads1115.interface.i2c_address,
            )?;

            Ok(Box::new(ads1115.channel3()))
        }

        // tmp36
        Device::Tmp36(tmp36) => {

            // Find referred adc sensor channel
            let config_sensor_adc_channel = config_sensors
                .iter()
                .find(|other_config_sensor| other_config_sensor.name == tmp36.interface.adc_channel)
                // Parsing validates already inputs and ensures that sensor with referred adc channel definition name already exists
                // Then it should never panic
                .unwrap();

            // Recreate parent adc channel
            // TODO validate that there is not any cycle graph
            let sensor_adc_channel =
                create_sensor_based_on_configuration(config_sensor_adc_channel, config_sensors)?;

            // Return result
            Ok(Box::new(Tmp36::init(
                config_sensor.name.clone(),
                config_sensor.description.clone(),
                sensor_adc_channel,
            )?))
        }
    }
}


// Initialize board sensors
pub fn init(
    config_sensors: Vec<ConfigSensor>
) -> Result<Vec<Box<dyn Sensor<Output = f64> + Send + Sync>>, Box<dyn std::error::Error>> {

    // Create vector to be filled with sensors
    let mut sensors = Vec::new();

    // Iterate over all passed sensor configuration definitions
    for config_sensor in &config_sensors {

        // Create sensor and initialize it
        let sensor = create_sensor_based_on_configuration(&config_sensor, &config_sensors)?;

        // Push sensor to array with all
        sensors.push(sensor);
    }


    // Return initialized sensors
    Ok(sensors)
}
