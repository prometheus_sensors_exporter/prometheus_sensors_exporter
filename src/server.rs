// Standard modules paths
use std::net::SocketAddr;
use std::sync::{Arc, Mutex};

// Custom modules paths
use crate::api::sensor::Sensor;

// External modules paths
use hyper::{header::CONTENT_TYPE, rt::Future, service::service_fn_ok, Body, Response, Server};
use log::error;
use prometheus::{Encoder, Gauge, Opts, Registry, TextEncoder};


fn tie_sensor_measurement_with_registry(
    sensor: &mut Box<dyn Sensor<Output = f64> + Send + Sync>,
    registry: &Registry,
) -> Result<(), String> {

    // Try to read measurement from sensor
    let measurement = sensor.read().map_err(|error| {

        format!(
            "Cannot get measurement from sensor {}: {}",
            sensor.name(),
            error
        )
    })?;

    // Try to create gauge
    let gauge =
        Gauge::with_opts(Opts::new(sensor.name(), sensor.description())).map_err(|error| {

            format!(
                "Cannot create gauge for sensor {}: {}",
                sensor.name(),
                error
            )
        })?;

    // Set gauge to measured value
    gauge.set(measurement);

    // Try to register gauge
    registry.register(Box::new(gauge)).map_err(|error| {

        format!(
            "Cannot register gauge for sensor {}: {}",
            sensor.name(),
            error
        )
    })?;

    // Return result
    Ok(())
}


fn populate_registry(
    registry: &Registry,
    sensors_sync: &Arc<Mutex<Vec<Box<dyn Sensor<Output = f64> + Send + Sync>>>>,
) {

    // Limit scope of lock
    {

        // Lock synchronized sensors to read measurements which mutate its content
        let mut sensors = sensors_sync.lock().unwrap();

        // Iterate over sensors to read measurement, then tie it with new gauge and register it
        for sensor in sensors.iter_mut() {

            // Try to tie sensor measurement with prometheus registry of indicators
            tie_sensor_measurement_with_registry(sensor, &registry)
                // Handle any error
                .unwrap_or_else(|error| {

                    // Cannot get measurement, so log it
                    error!(
                        "Cannot tie sensor with registry {}: {}",
                        sensor.name(),
                        error
                    );
                });
        }
    }
}

pub fn run(
    socket_address: SocketAddr,
    sensors: Vec<Box<dyn Sensor<Output = f64> + Send + Sync>>,
) {

    // Wrap board into synchronized structure
    let sensors_sync = Arc::new(Mutex::new(sensors));

    let new_service = move || {

        // Create individual instace of synchronized board structure for each thread
        let sensors_sync = Arc::clone(&sensors_sync);

        service_fn_ok(move |_request| {

            // Create registry
            let registry = Registry::new();

            // Populate registry indicators with values measured from sensors
            populate_registry(&registry, &sensors_sync);

            // Gather the metrics
            let metric_families = registry.gather();

            // Encode data into buffer
            let mut buffer = vec![];

            let encoder = TextEncoder::new();

            encoder.encode(&metric_families, &mut buffer).unwrap();

            // Wrap buffer into http response
            let response = Response::builder()
                .status(200)
                .header(CONTENT_TYPE, encoder.format_type())
                .body(Body::from(buffer))
                .unwrap();

            // Return built response
            response
        })
    };

    let server = Server::bind(&socket_address)
        .serve(new_service)
        .map_err(|e| eprintln!("Server error: {}", e));

    hyper::rt::run(server);
}
