// Validate input arguments


// Internal modules paths
use crate::api::config::ConfigSensor;
use crate::api::config::Device;

// Validate inputs
pub fn validate(sensors: &Vec<ConfigSensor>) -> Result<(), String> {

    // Ensure that at least one mandatory sensor definition is passed
    if sensors.is_empty() {

        return Err(String::from(
            "Error: Command line arguments have to define at least one mandatory sensor\n",
        ));
    }

    // Ensure that all sensors names are unique
    validate_sensors_unique_names(sensors)?;

    // Ensure that all analog sensors have assigned existing channel on previously defined adc sensor
    validate_analog_sensors_assigned_channel(sensors)?;

    // Prevent cycle graph of chained analog sensors
    validate_analog_sensors_are_not_chained(sensors)?;

    // Validation is fine
    Ok(())
}


// Ensure that all sensors names are unique
fn validate_sensors_unique_names(sensors: &Vec<ConfigSensor>) -> Result<(), String> {

    // Iterate over all sensors
    for (index, sensor) in sensors.iter().enumerate() {

        // Create slice containing only rest of sensors
        let rest_of_sensors = &sensors[index + 1..];

        // Ensure that current sensor name is not repeated with any other
        if rest_of_sensors
            .iter()
            .any(|other_sensor| other_sensor.name == sensor.name)
        {

            return Err(format!(
                "All sensors name have to be unique, but at least one is not: '{}'\n",
                sensor.name
            ));
        }
    }

    // Validation is fine
    Ok(())
}


// Ensure that all analog sensors have assigned existing channel on previously defined adc sensor
fn validate_analog_sensors_assigned_channel(sensors: &Vec<ConfigSensor>) -> Result<(), String> {

    // Iterate over all sensors
    for sensor in sensors {

        // Check if sensor is analog one
        if let Device::Tmp36(tmp36) = &sensor.device {

            // Ensure that referred adc channel name is defined already
            sensors
                // Get iterator
                .iter()
                // Try to find sensor with referred name
                .find(|other_sensor| other_sensor.name == tmp36.interface.adc_channel)
                // Convert resulted option into result type with defined error in case of failure
                .ok_or(format!(
                    "Analog sensor '{}' is referring to adc channel of not defined sensor '{}'\n",
                    sensor.name, tmp36.interface.adc_channel
                ))
                // Get rid of found sensor and return Ok(()) (as in function prototype) in case of success
                .map(|_| ())?;
        }
    }

    // Validation is fine
    Ok(())
}


// It could be interesting to have chain of processing with many parents levels
// But such processing it is easier and more flexible to model in final grafana expression
// So to simplify design analog sensor cannot be connected to another analog sensor in chain
// It prevents to make graph cycle
fn validate_analog_sensors_are_not_chained(sensors: &Vec<ConfigSensor>) -> Result<(), String> {

    // Iterate over all sensors
    for sensor in sensors {

        // Check if sensor is analog one
        if let Device::Tmp36(tmp36) = &sensor.device {

            // Find referred adc channel sensor
            let referred_adc_channel = sensors
                // Get iterator
                .iter()
                 // Try to find sensor with referred name
                .find( |other_sensor| { other_sensor.name == tmp36.interface.adc_channel } )
                // Convert resulted option into result type with defined error in case of failure
                // Error should not happen as it is already validated in previous test
                // But keep check instead of unwrap() just in case of some code changes
                .ok_or_else(|| { return format!("Analog sensor '{}' is referring to adc channel of not defined sensor '{}'\n", sensor.name, tmp36.interface.adc_channel) } )?;

            // Ensure that referred sensor is not another chained analog sensor
            if let Device::Tmp36(_) = &referred_adc_channel.device {

                // Analog sensors cannot be chained
                return Err( format!("Analog sensor '{}' should refer to adc channel but it is referring to another chained analog sensor '{}'\n", sensor.name, referred_adc_channel.name) );
            }
        }
    }

    // Validation is fine
    Ok(())
}
