// Parse configuration reading command line arguments and environment variables


// Standard modules paths
use std::net::AddrParseError;
use std::net::SocketAddr;
use std::process::exit;

// External modules paths
use log::debug;
use structopt::StructOpt;

// Internal modules paths
use crate::api::config::Config;
use crate::api::config::ConfigSensor;
use crate::api::error::parse::CliError;

#[derive(Debug, PartialEq, StructOpt)]

pub struct ConfigGeneric {
    /// Address of interface to bind
    #[structopt(long, default_value = "0.0.0.0", env = "SOCKET_INTERFACE")]
    socket_interface: String,

    /// Number in decimal
    #[structopt(long, default_value = "9898", env = "SOCKET_PORT")]
    socket_port: u16,

    /// Test correctness of passed command line arguments
    #[structopt(long, env = "DRY_RUN")]
    dry_run: bool,

    // Field is not parsed
    // It is only for more accurate library help message
    sensor: Vec<String>,
}


fn print_version() {

    // Cargo sets some variables during compilation
    let package = env!("CARGO_PKG_NAME");

    let version = env!("CARGO_PKG_VERSION");

    println!("{} {}", package, version);
}


fn print_help() {

    print_version();

    print!("
USAGE:
    prometheus_sensors_exporter [OPTIONS]... ( SENSOR )...

WHERE:
    SENSOR = sensor --name <name> --description <description> DEVICE
    DEVICE = ( tmp102 I2C | si7021-temperature I2C | si7021-humidity I2C | ads1115-channel0 I2C | ads1115-channel1 I2C | ads1115-channel2 I2C | ads1115-channel3 I2C | tmp36 ADC )
    I2C = --i2c-bus <bus> [ --i2c-address <address> ]
    SPI = --spi-bus <bus> [ --spi-address <address> ]
    ADC = --adc-channel <name of previously defined adc sensor channel>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
        --dry-run    Test correctness of passed command line arguments

OPTIONS:
        --socket-interface <socket-interface>        Address of interface to bind [default: 0.0.0.0]
        --socket-port <socket-port>                  Number in decimal [default: 9898]
")
}


fn handle_possible_parameters_help_version(arguments: &Vec<String>) {

    // Check if user wants to print help message
    if arguments
        .iter()
        .any(|parameter| parameter == "--help" || parameter == "-h")
    {

        // Print custom message
        print_help();

        // Exit successfully
        exit(0);
    }

    // Check if user wants to print version message
    if arguments
        .iter()
        .any(|parameter| parameter == "--version" || parameter == "-V")
    {

        // Print custom message
        print_version();

        // Exit successfully
        exit(0);
    }
}


pub fn parse(args: std::env::Args) -> Result<Config, CliError> {

    // Collect parameters iterator into vector
    let arguments: Vec<String> = args.collect();

    // If --help or --version parameters are passed, then handle them and exit successfully
    handle_possible_parameters_help_version(&arguments);

    // Structopt does not support parsing many subcommands at once
    // Clap v3 neither
    //
    // So it is implemented here by splitting all parameters by 'sensor' word delimiter
    // and then parsing parameters subgroups with library
    //
    // https://github.com/clap-rs/clap/issues/1704

    // Split command line arguments into groups separated by 'sensor' delimiter
    let mut parameters_groups = arguments.split(|argument| argument == "sensor");

    // First group of parameters is basic without any information for specific sensor
    let generic_parameters = parameters_groups
        .next()
        // There is always first group before even non existing delimiter
        .unwrap();

    // Try to parse generic parameters
    let generic = ConfigGeneric::from_iter_safe(generic_parameters)
        // Convert possible error into string if parsing fails
        .map_err(|error| error.to_string())?;

    // Print parsed structure for debug purpose
    debug!("{:#?}", generic);

    // Create vector to be filled with all parsed sensors
    let mut sensors = Vec::new();

    // Iterate over rest of groups each containing complete sensor information
    for sensor_parameters in parameters_groups {

        // OptSensor::from_iter is going to skip first item, so it have to be put anything to be skipped
        // TODO maybe StructOpt::from_iter should not skip first item to not have to workaround it?
        let array_with_empty_item = [String::from("")];

        let sensor_parameters_with_extra_empty_first_item =
            array_with_empty_item.iter().chain(sensor_parameters);

        // Parse all optional sensor parameters
        let sensor = ConfigSensor::from_iter_safe(sensor_parameters_with_extra_empty_first_item)
            // Convert possible error into string if parsing fails
            .map_err(|error| error.to_string())?;

        // Fill sensors vector
        sensors.push(sensor);
    }

    // Print parsed structure for debug purpose
    debug!("{:#?}", sensors);

    // Parse socket details and try to construct it
    let socket = parse_socket_address(
        generic.socket_interface.clone(),
        generic.socket_port.clone(),
    )?;

    // Return result
    Ok(Config {
        socket: socket,
        dry_run: generic.dry_run,
        sensors: sensors,
    })
}


fn parse_socket_address(
    interface: String,
    port: u16,
) -> Result<SocketAddr, AddrParseError> {

    let socket = format!("{}:{}", interface, port)
        .parse()?;

    // Return result
    Ok(socket)
}
