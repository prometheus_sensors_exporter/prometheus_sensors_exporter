// Take care about passed configuration


// Standard modules paths
use std::process::exit;

// Internal modules paths
use crate::api::config::Config;
use crate::api::error::parse::CliError;


// Declare modules
pub mod parse;
pub mod validate;


fn exit_if_dry_run(dry_run: bool) {

    // Check if --dry-run flag is enabled
    if dry_run {

        // Inform user
        println!("INFO: command line arguments are parsed fine");
        println!("INFO: exiting due to enabled --dry-run flag");

        // Exit successfully
        exit(0);
    }
}


pub fn parse_and_validate(args: std::env::Args) -> Result<Config, CliError> {

    // Parse command line arguments
    let config = parse::parse(args)?;

    // Validate created sensors based on input data
    validate::validate(&config.sensors)?;

    // Inform user about server listening interface and port
    println!("INFO: address to be listen: {:?}", config.socket);

    // Stop any further processing and exit successfully here if --dry-run flag is enabled
    exit_if_dry_run(config.dry_run);

    // Return result
    Ok(config)
}
