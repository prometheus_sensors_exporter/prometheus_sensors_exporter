// External modules paths
use crate::external::si7021;
use linux_embedded_hal as hal;

// Standard modules paths
use std::sync::{Arc, Mutex};

// Internal modules paths
use humidity::Humidity;
use temperature::Temperature;

// Import modules
pub mod humidity;
pub mod temperature;


fn get_i2c_address(i2c_address: Option<u8>) -> Result<(), String> {

    // Figure out i2c address based on provided sensor configuration
    match i2c_address {
        // User specified address
        Some(i2c_address) => {

            // Check if provided address is fine
            match i2c_address {
                0x40 => Ok(()),
                _ => Err( format!( "Provided i2c address '{}' for si7021 sensor is out of range. Supported address is only 0x40", i2c_address ) ),
            }
        }
        // Address is not specified, so use default one
        None => Ok(()),
    }
}


pub fn init(
    name: String,
    description: String,
    i2c_bus: usize,
    i2c_address: Option<u8>,
) -> Result<Initialized, String> {

    // Shadow variable and build system path to choosen i2c bus
    let i2c_bus = format!("/dev/i2c-{}", i2c_bus);

    // Initialize i2c communication bus
    let dev = hal::I2cdev::new(i2c_bus).map_err(|e| {

        format!(
            "Cannot initialize i2c bus for si7021 temperature sensor: {}",
            e.to_string()
        )
    })?;

    // Ensure that optionally provided address is correct one
    get_i2c_address(i2c_address)?;

    // Initialize sensor
    let sensor = si7021::Si7021::new(dev);

    // Return initialized object
    Ok(Initialized::new(name, description, sensor))
}


pub struct Initialized {
    sensor: Arc<Mutex<si7021::Si7021<hal::I2cdev>>>,
    name: String,
    description: String,
}


impl Initialized {
    fn new(
        name: String,
        description: String,
        sensor: si7021::Si7021<hal::I2cdev>,
    ) -> Self {

        Initialized {
            name,
            description,
            sensor: Arc::new(Mutex::new(sensor)),
        }
    }

    pub fn temperature(&mut self) -> Temperature {

        Temperature::new(Self {
            name: self.name.clone(),
            description: self.description.clone(),
            sensor: Arc::clone(&self.sensor),
        })
    }

    pub fn humidity(&mut self) -> Humidity {

        Humidity::new(Self {
            name: self.name.clone(),
            description: self.description.clone(),
            sensor: Arc::clone(&self.sensor),
        })
    }
}
