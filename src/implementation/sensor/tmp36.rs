// Internal modules paths
use crate::api::sensor::Sensor;


pub struct Tmp36 {
    sensor: Box<dyn Sensor<Output = f64> + Send + Sync>,
    name: String,
    description: String,
}

impl Tmp36 {
    pub fn init(
        name: String,
        description: String,
        sensor: Box<dyn Sensor<Output = f64> + Send + Sync>,
    ) -> Result<Tmp36, String> {

        // Return self object
        Ok(Tmp36 {
            name,
            description,
            sensor,
        })
    }
}

impl Sensor for Tmp36 {
    type Output = f64;

    fn name(&self) -> String {

        self.name.clone()
    }

    fn description(&self) -> String {

        self.description.clone()
    }

    fn read(&mut self) -> Result<Self::Output, String> {

        // Read measured temperature from sensor
        self.sensor
            .read()
            // Convert result into float if case of success
            .map(|result| {

                // Convert result to wide float
                result as f64
            // full range of sensor measurement is from -50 to 125 celcius degree
            * (125.0 + 50.0)
            // maximum measurement for 125 celcius degree is 1750 mV
            / 1750.0
            // substract minimum possible measurement -50 celcius degree
            - 50.0
            })
            // In case of failure convert error type into String
            .map_err(|error| format!("{:?}", error))
    }
}
