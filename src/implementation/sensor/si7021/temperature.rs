// Internal modules paths
use super::Initialized;
use crate::api::sensor::Sensor;


pub struct Temperature {
    si7021: Initialized,
}

impl Temperature {
    pub fn new(si7021: Initialized) -> Self {

        Self {
            si7021,
        }
    }
}

impl Sensor for Temperature {
    type Output = f64;

    fn name(&self) -> String {

        self.si7021.name.clone()
    }

    fn description(&self) -> String {

        self.si7021.description.clone()
    }

    fn read(&mut self) -> Result<f64, String> {

        // Limit scope of lock
        {

            // Lock sensor
            let mut sensor = self
                .si7021
                .sensor
                .lock()
                // Should never panic when locking mutex
                // as it would mean that another thread which holds lock panicked already
                // and then program logic is buggy
                // so exit immediately
                .unwrap();

            // Get measurement from sensor
            sensor
                .temperature()
                // Convert result into float if case of success
                .map(|result| result as f64 / 100.0)
                // In case of failure convert error type into String
                .map_err(|error| format!("{:?}", error))
        }
    }
}
