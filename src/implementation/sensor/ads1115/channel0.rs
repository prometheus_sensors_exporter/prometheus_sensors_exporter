// External modules paths
use ads1x1x::channel;
use embedded_hal::adc::OneShot;

// Internal modules paths
use super::Initialized;
use crate::api::sensor::Sensor;


pub struct Channel {
    ads1115: Initialized,
}

impl Channel {
    pub fn new(ads1115: Initialized) -> Self {

        Self {
            ads1115,
        }
    }
}

impl Sensor for Channel {
    type Output = f64;

    fn name(&self) -> String {

        self.ads1115.name.clone()
    }

    fn description(&self) -> String {

        self.ads1115.description.clone()
    }

    fn read(&mut self) -> Result<f64, String> {

        // Limit scope of lock
        {

            // Lock sensor
            let mut sensor = self
                .ads1115
                .sensor
                .lock()
                // Should never panic when locking mutex
                // as it would mean that another thread which holds lock panicked already
                // and then program logic is buggy
                // so exit immediately
                .unwrap();

            // Get measurement from sensor
            block!(sensor.read(&mut channel::SingleA0))
                // Convert result into float in case of success
                .map(|result| result as f64 * self.ads1115.scale_raw_to_minivolts as f64)
                // In case of failure convert error type into String
                .map_err(|error| format!("{:?}", error))
        }
    }
}
