// External modules paths
use ads1x1x::{Ads1x1x, Error::InvalidInputData, Error::I2C, FullScaleRange};
use linux_embedded_hal as hal;

// Standard modules paths
use std::sync::{Arc, Mutex};

// Import modules
pub mod channel0;
pub mod channel1;
pub mod channel2;
pub mod channel3;


fn get_scale_raw_to_milivolts(voltage_range: ads1x1x::FullScaleRange) -> f32 {

    // Define local helper function
    fn calculate_scale_raw_to_milivolts(voltage_range: f32) -> f32 {

        // Operate on input argument
        voltage_range
        // Sensor accurancy is 15 bits for non-diferential, grounded second input
        / (2 as u32).pow(15) as f32
        // Express result in miliVolts
        * 1000.0
    }

    // Map input voltage range type into corresponding number and calculate scale factor
    match voltage_range {
        FullScaleRange::Within6_144V => calculate_scale_raw_to_milivolts(6.144),
        FullScaleRange::Within4_096V => calculate_scale_raw_to_milivolts(4.096),
        FullScaleRange::Within2_048V => calculate_scale_raw_to_milivolts(2.048),
        FullScaleRange::Within1_024V => calculate_scale_raw_to_milivolts(1.024),
        FullScaleRange::Within0_512V => calculate_scale_raw_to_milivolts(0.512),
        FullScaleRange::Within0_256V => calculate_scale_raw_to_milivolts(0.256),
    }
}


fn get_i2c_address(i2c_address: Option<u8>) -> Result<ads1x1x::SlaveAddr, String> {

    // Figure out i2c address based on provided sensor configuration
    match i2c_address {
        // User specified address
        Some(i2c_address) => {

            // Check if provided address is fine
            match i2c_address {
                    // Allowed values
                    // TODO verify alternative bool values for number
                    0x48 => Ok( ads1x1x::SlaveAddr::Default ),
                    0x49 => Ok( ads1x1x::SlaveAddr::Alternative(false, true) ),
                    0x4A => Ok( ads1x1x::SlaveAddr::Alternative(true, false) ),
                    0x4B => Ok( ads1x1x::SlaveAddr::Alternative(true, false) ),
                    // Out of supported range
                    _ => Err( format!( "Provided i2c address '{}' for ads1115 sensor is out of range. Supported addresses are 0x48, 0x49, 0x4A, 0x4B", i2c_address ) ),
            }
        }
        // Address is not specified, so use default one
        None => Ok(ads1x1x::SlaveAddr::default()),
    }
}


pub fn init(
    name: String,
    description: String,
    i2c_bus: usize,
    i2c_address: Option<u8>,
) -> Result<Initialized, String> {

    // Hardcode used range
    let range = ads1x1x::FullScaleRange::Within4_096V;

    // Shadow variable and build system path to choosen i2c bus
    let i2c_bus = format!("/dev/i2c-{}", i2c_bus);

    // Initialize i2c communication bus
    let dev = hal::I2cdev::new(i2c_bus).map_err(|e| {

        format!(
            "Cannot initialize i2c bus for ads1115 adc sensor: {}",
            e.to_string()
        )
    })?;

    // Try to map optionally passed address number into possible supported values
    let i2c_address = get_i2c_address(i2c_address)?;

    // Initialize sensor
    let mut sensor = Ads1x1x::new_ads1115(dev, i2c_address);

    // All input sensors are supplied by 5 Volts
    // So set gain amplifier to have measurements in range of 6 Volts
    sensor.set_full_scale_range(range).map_err(|e| {
        match e {
            I2C(e) => format!("Cannot initialize ads1115 adc sensor because of some i2c bus communication failure: {}", e.to_string()),
            InvalidInputData => String::from("Cannot initialize ads1115 adc sensor because of invalid input data"),
        }
    })?;

    // Return initialized object
    Ok(Initialized::new(
        name,
        description,
        sensor,
        get_scale_raw_to_milivolts(range),
    ))
}


pub struct Initialized {
    name: String,
    description: String,
    sensor: Arc<
        Mutex<
            ads1x1x::Ads1x1x<
                ads1x1x::interface::I2cInterface<hal::I2cdev>,
                ads1x1x::ic::Ads1115,
                ads1x1x::ic::Resolution16Bit,
                ads1x1x::mode::OneShot,
            >,
        >,
    >,
    scale_raw_to_minivolts: f32,
}

impl Initialized {
    fn new(
        name: String,
        description: String,
        sensor: ads1x1x::Ads1x1x<
            ads1x1x::interface::I2cInterface<hal::I2cdev>,
            ads1x1x::ic::Ads1115,
            ads1x1x::ic::Resolution16Bit,
            ads1x1x::mode::OneShot,
        >,
        scale_raw_to_minivolts: f32,
    ) -> Self {

        Initialized {
            name,
            description,
            sensor: Arc::new(Mutex::new(sensor)),
            scale_raw_to_minivolts,
        }
    }

    #[allow(dead_code)]

    pub fn channel0(&mut self) -> channel0::Channel {

        channel0::Channel::new(Self {
            name: self.name.clone(),
            description: self.description.clone(),
            sensor: Arc::clone(&self.sensor),
            scale_raw_to_minivolts: self.scale_raw_to_minivolts,
        })
    }

    #[allow(dead_code)]

    pub fn channel1(&mut self) -> channel1::Channel {

        channel1::Channel::new(Self {
            name: self.name.clone(),
            description: self.description.clone(),
            sensor: Arc::clone(&self.sensor),
            scale_raw_to_minivolts: self.scale_raw_to_minivolts,
        })
    }

    #[allow(dead_code)]

    pub fn channel2(&mut self) -> channel2::Channel {

        channel2::Channel::new(Self {
            name: self.name.clone(),
            description: self.description.clone(),
            sensor: Arc::clone(&self.sensor),
            scale_raw_to_minivolts: self.scale_raw_to_minivolts,
        })
    }

    #[allow(dead_code)]

    pub fn channel3(&mut self) -> channel3::Channel {

        channel3::Channel::new(Self {
            name: self.name.clone(),
            description: self.description.clone(),
            sensor: Arc::clone(&self.sensor),
            scale_raw_to_minivolts: self.scale_raw_to_minivolts,
        })
    }
}
