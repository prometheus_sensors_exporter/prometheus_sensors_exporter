// External modules paths
use linux_embedded_hal as hal;
use tmp1x2::Tmp1x2;

// Internal modules paths
use crate::api::sensor::Sensor;

pub struct Tmp102 {
    sensor: tmp1x2::Tmp1x2<hal::I2cdev, tmp1x2::marker::mode::Continuous>,
    name: String,
    description: String,
}

impl Tmp102 {
    fn get_i2c_address(i2c_address: Option<u8>) -> Result<tmp1x2::SlaveAddr, String> {

        // TODO maybe change interface of tmp1x2 crate to avoid dealing with numbers here?
        // Figure out i2c address based on provided sensor configuration
        match i2c_address {
            // User specified address
            Some(i2c_address) => {

                // Check if provided address is fine
                match i2c_address {
                    // Add0 pin is connected to ground
                    0x48 => Ok( tmp1x2::SlaveAddr::Default ),
                    // Add0 pin is connected to voltage
                    0x49 => Ok( tmp1x2::SlaveAddr::Alternative(false, true) ),
                    // Add0 pin is connected to sda
                    0x4A => Ok( tmp1x2::SlaveAddr::Alternative(true, false) ),
                    // Add0 pin is connected to scl
                    0x4B => Ok( tmp1x2::SlaveAddr::Alternative(true, false) ),
                    // Out of supported range
                    _ => Err( format!( "Provided i2c address '{}' for tmp102 sensor is out of range. Supported addresses are 0x48, 0x49, 0x4A, 0x4B", i2c_address ) ),
                }
            }
            // Address is not specified, so use default one
            None => Ok(tmp1x2::SlaveAddr::default()),
        }
    }

    /// Check optional parameter and if passed, then use alternative i2c address 0x49
    /// If passed 0x48, then keep it
    /// If not passed any, then use default one 0x48
    /// NOTE: Behaviour could change in future

    pub fn init(
        name: String,
        description: String,
        i2c_bus: usize,
        i2c_address: Option<u8>,
    ) -> Result<Self, String> {

        // Shadow variable and build system path to choosen i2c bus
        let i2c_bus = format!("/dev/i2c-{}", i2c_bus);

        // Initialize i2c communication bus
        let dev = hal::I2cdev::new(i2c_bus).map_err(|e| {

            format!(
                "Cannot initialize i2c bus for tmp102 temperature sensor: {}",
                e.to_string()
            )
        })?;

        // Try to map optionally passed address number into possible supported values
        let i2c_address = Tmp102::get_i2c_address(i2c_address)?;

        // Initialize sensor
        let sensor = Tmp1x2::new(dev, i2c_address);

        // Return self object
        Ok(Self {
            name,
            description,
            sensor,
        })
    }
}

impl Sensor for Tmp102 {
    type Output = f64;

    fn name(&self) -> String {

        self.name.clone()
    }

    fn description(&self) -> String {

        self.description.clone()
    }

    fn read(&mut self) -> Result<f64, String> {

        // Read measured temperature from sensor
        self.sensor
            .read_temperature()
            // Convert result into float if case of success
            .map(|result| result as f64)
            // In case of failure convert error type into String
            .map_err(|error| format!("{:?}", error))
    }
}
